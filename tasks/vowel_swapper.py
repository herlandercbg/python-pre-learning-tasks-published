def vowel_swapper(string):
    # ==============
    dictionary = {"a": "4", "e": "3", "i": "!", "o": "ooo", "u": "|_|", "O": "000"}
    output = ""
    for element in string:
        if element in dictionary.keys():
            output += dictionary[element]
        elif element.lower() in dictionary.keys():
            if element == "O":
                output += dictionary[element]
            else:
                output += dictionary[element.lower()]
        else:
            output += element
    return output
    # ==============

print(vowel_swapper("aA eE iI oO uU")) # Should print "44 33 !! ooo000 |_||_|" to the console
print(vowel_swapper("Hello World")) # Should print "H3llooo Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "3v3ryth!ng's 4v4!l4bl3" to the console
